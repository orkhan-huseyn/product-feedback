import classNames from 'classnames';
import classes from './Button.module.css';

function Button({ children, color, ...props }) {
  const buttonClassName = classNames(classes.Button, {
    [classes['Button--purple']]: color === 'purple',
    [classes['Button--blue']]: color === 'blue',
    [classes['Button--black']]: color === 'black',
    [classes['Button--red']]: color === 'red',
  });

  return (
    <button className={buttonClassName} {...props}>
      {children}
    </button>
  );
}

export default Button;
