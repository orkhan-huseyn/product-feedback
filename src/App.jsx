import { useState } from 'react';
import Button from './components/Button';
import Dropdown from './components/Dropdown';

const dropdownOptions = [
  'Most upvotes',
  'Least upvotes',
  'Most comments',
  'Least comments',
];

function App() {
  const [buttonText, setButtonText] = useState('Select option');

  function handleChange(option) {
    setButtonText(option);
  }

  return (
    <div>
      <Button color="purple">Click me!</Button>
      <Button color="blue">Click me!</Button>
      <Button color="black">Click me!</Button>
      <Button color="red">Click me!</Button>
      <hr />
      <Dropdown
        options={dropdownOptions}
        onChange={handleChange}
        trigger={({ handleClick }) => (
          <Button color="black" onClick={handleClick}>
            {buttonText}
          </Button>
        )}
      />
    </div>
  );
}

export default App;
